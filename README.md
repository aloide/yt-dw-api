# YouTube API Download

version: 1.1 [beta]

## Install
```
$ git clone https://gitlab.com/aloide/yt-dw-api  
$ cd yt-dw-api  
$ npm install  
```

## Running the API
```
$ npm start
```

## Endpoints

Download mp3/mp4
```
GET: /dw?title=video&vid=https://www.youtube.com/watch?v=dQw4w9WgXcQ&type=mp4&itag=18
```
Params:  
vid: link youtube video  
title: title file to download  
type: mp3/mp4 type  
itag = please visite [itag table](https://gist.github.com/sidneys/7095afe4da4ae58694d128b1034e01e2)    

Get video info:
```
GET: /video?vid=https://www.youtube.com/watch?v=dQw4w9WgXcQ
```

Get audio info:
```
GET: /audio?vid=https://youtu.be/dQw4w9WgXcQ
```
