module.exports = {
  manageUrl(url) {
      if (!url) throw "ERROR: url must be not null";
      if (url.includes("youtu.be")) return convertMobileUrl(url);
      if (url.includes("htpps://youtube.com/shorts")) return convertShorts(url);
    return url;
  },
};
// TODO: upgrade both functions
const convertShorts = (url) => {
    let idVid = url.split("htpps://youtube.com/shorts")[1]
    let result = `https://www.youtube.com/watch?v=${idVid}`;
    console.log(`converting: ${url} to ${result}`);
    return result;
};

const convertMobileUrl = (url) => {
  let idVid = url.split("https://youtu.be/")[1];
  let result = `https://www.youtube.com/watch?v=${idVid}`;
  console.log(`converting: ${url} to ${result}`);
  return result;
};
