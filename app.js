const express = require("express");
//const fs = require("fs");
const ytdl = require("ytdl-core");
const tools = require('./UrlTools')
const cors = require('cors')

const app = express();
app.use(cors())
const PORT = 3010;
//const link = "https://www.youtube.com/watch?v=ONyrc0jHb3A"


// test endpoint
app.get("/test", (req, res) => {
  const vid = req.query.vid;
  res.send(tools.manageUrl(vid));
  //ytdl(link).pipe(fs.createWriteStream("video.mp4"));
  //res.send("hi");
});

// get all data from video
app.get("/video", async (req, res) => {
  const vid = tools.manageUrl(req.query.vid);
  console.log("> querying video: " + vid);
  let info = await ytdl.getInfo(vid).catch((e) => {
    console.log(400),
      res.status(400).json({ message: "400 bad request: " + vid });
    return;
  });
  if (info) {
    console.log(200);
    res.status(200).json(info);
  }
});

// get all data audio from video
app.get("/audio", async (req, res) => {
  const vid = tools.manageUrl(req.query.vid);
  let result;
  let info;
  console.log("> querying audio: " + vid);
  try {
    info = await ytdl.getInfo(vid);
  } catch (e) {
    res.status(400).json({ message: "400 bad request: " + vid + ". " + e });
    return;
  }
  if (!info) {
    res.status(500).json({ message: "500 something is wrong: " + vid });
    return;
  } else {
    result = ytdl.filterFormats(info.formats, "audioonly");
    console.log(200);
    res.status(200).json(result);
  }
});

// dw title and quality
app.get("/dw", async(req, res) => {
    const vid = tools.manageUrl(req.query.vid)
    const {itag, title, type} = req.query;
    console.log("> downloading: " + req.originalUrl );

    if(type == "mp4"){
        res.header('Content-Disposition', `attachment; filename=${title}.mp4`)
        res.header("Content-Type, 'video/mp4")
    }
    if(type == "mp3"){
        res.header('Content-Disposition', `attachment; filename=${title}.mp3`)
        res.header("Content-Type, 'audio/mp3")
    }

    try{
        ytdl(vid, {filter: format => format.itag == itag} ).pipe(res)
    }
    catch(e){
        res.send("something is broken: " + e)
    }

  });


app.listen(PORT, console.log("running..."));
